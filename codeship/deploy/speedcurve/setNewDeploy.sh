#!/bin/bash

#Call the SpeedCurve API to trigger a new deployment test marker
speedCurveAPIKey=OW50YXFjOHA1Nml5OTJpZmtkZXUxZ2M0ejBsaHI3Ong=
speedCurveSiteId=301387
buildNumber=$CI_BUILD_NUMBER
buildURL=$CI_BUILD_URL


curl --request POST --url "https://api.speedcurve.com/v1/deploys?note=${buildNumber}&detail=${buildURL}&site_id=${speedCurveSiteId}" --header "authorization: Basic ${speedCurveAPIKey}"  --header 'cache-control: no-cache'
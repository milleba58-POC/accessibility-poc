#!/bin/bash
#Azure CLI Commands

#Azure Service Principal Designed for CodeShip Access to Barry Miller's Azure Test Account
#Variable values should be stored within Environment Tab once working
az login --service-principal --username f8d1ce27-594d-4b78-a1df-9ba8f8125d57 --password 799c2977-3f1f-49fa-95cc-82287e3beecd --tenant cf134d4e-95cc-4ff6-9de2-73b75bcf5f34

# sample webapp name
webappname=codeshipwebapp
webappStagingSlotName=staging
resourceName=CodeShipResourceGroup
appServiceName=CodeShipAppServicePlan

# Swap the verified/warmed up staging slot into production.
az webapp deployment slot swap --name $webappname --resource-group $resourceName --slot $webappStagingSlotName

# Copy the result of the following command into a browser to see the web app in the production slot.
echo http://$webappname.azurewebsites.net
#!/bin/bash
#Azure CLI Commands

#Azure Service Principal Designed for CodeShip Access to Barry Miller's Azure Test Account
#Variable values should be stored within Environment Tab once working
az login --service-principal --username f8d1ce27-594d-4b78-a1df-9ba8f8125d57 --password 799c2977-3f1f-49fa-95cc-82287e3beecd --tenant cf134d4e-95cc-4ff6-9de2-73b75bcf5f34

# sample webapp name
webappname=codeshipwebapp
webappStagingSlotName=staging
resourceName=CodeShipResourceGroup
appServiceName=CodeShipAppServicePlan

# Make sure the CodeShipResource Group is Available
az group create --location westeurope --name $resourceName

# Create an App Service plan in `S1` tier, so that deployment slot can be created later.
az appservice plan create --name $appServiceName --resource-group $resourceName --sku S1

# Create a web app.
az webapp create --name $webappname --resource-group $resourceName --plan $appServiceName

#Create a deployment slot.
az webapp deployment slot create --name $webappname --resource-group $resourceName --slot $webappStagingSlotName


# Get FTP publishing profile and query for publish URL and credentials
creds=($(az webapp deployment list-publishing-profiles --name $webappname --resource-group $resourceName --slot $webappStagingSlotName --query "[?contains(publishMethod, 'FTP')].[publishUrl,userName,userPWD]" --output tsv))

# Use lftp to perform FTP upload, because this is the CodeShip recommended ftp tool
# exlude codeship and git folder
lftp -c "open -u ${creds[1]},${creds[2]} ${creds[0]}; set ssl:verify-certificate no; mirror --exclude codeship/ --exclude-glob *.git -R ~/clone/ /site/wwwroot/"

# Copy the result of the following command into a browser to see the static HTML site.
echo http://$webappname-staging.azurewebsites.net